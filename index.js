const btnStop = document.querySelector("#btn-stop");
const btnStart = document.querySelector("#btn-start");

let slideIndex = 0;
showSlides();

let interval = setInterval(showSlides, 3000);

btnStop.addEventListener("click", () => {
    clearInterval(interval);
    btnStart.removeAttribute("disabled");
    btnStop.setAttribute("disabled", true);
});
btnStart.addEventListener("click", () => {
    setInterval(showSlides, 3000);
    btnStart.setAttribute("disabled", true);
    btnStop.removeAttribute("disabled");
});

function showSlides() {
    const slides = document.getElementsByClassName("image-to-show");

    for (let i = 0; i < slides.length; i++) {
        slides[i].classList.add("hidden");
    }

    slideIndex++;

    if (slideIndex > slides.length) slideIndex = 1;
        slides[slideIndex - 1].classList.remove("hidden");
}